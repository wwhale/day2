console.log('--Exercise 3.4.1 ---');

class Employee {
    constructor(
        firstName,
        lastName,
        employeeNumber,
        salary, 
        age,
        department
    ) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.employeeNumber = employeeNumber;
        this.salary = salary;
        this.age = age;
        this.department = department;
    }

    getFullName(){
        return this.firstName + " " + this.lastName;
    }

    isRich(){
        if (this.salary > 2000) {
            return true
        } else {
            return false
        }
    }

    belongsToDepartment(dep) {
        if (this.department == dep) {
            return this.department + " department.";
        } else {
            return this.department + " department." + " Is NOT in " + dep + " department.";
        }
    }
};

staff = new Employee("Fname","Lname",123,200,33,"IT");


console.log(staff);
console.log(staff.getFullName());

console.log(staff.getFullName() + " is rich: is " + staff.isRich() + ". Because Salary is S$" +staff.salary);

console.log(staff.getFullName() + " is in " + staff.belongsToDepartment("HR"));