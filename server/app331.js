console.log('--Exercise 3.3.1 ---');

var num = 2;
var waitFor = new Promise((resolve, reject)=>{
    setTimeout(resolve, 3000);
}).then(()=>{

    if(num >18){
        console.log("OK !");
    }else{
        throw new Error("NOT OK !");
    }
    
}).catch((error)=>{
    console.log(error);
    console.log("error !");
});


console.log('--Exercise 3.3.1 sample 2 ---');

function successCallback(){
    console.log("success ! The number is greater than 18.");
}
    
function errorCallback(){
    console.log("error. The number is less than or equal to 18");
}


function result(x,successCallback ,errorCallback){
    if(x>18){
        successCallback();
    }else{
        errorCallback();
    }

}
    
result(1, successCallback,errorCallback);